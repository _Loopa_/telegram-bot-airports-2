import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


# Link for test case: http://google.com
def airport_picture(name):
    options = webdriver.ChromeOptions()
    capabilities = options.to_capabilities()
    driver = webdriver.Remote(command_executor='http://selenium:4444/wd/hub', desired_capabilities=capabilities)

    driver.get("https://www.google.com")
    time.sleep(1)
    driver.save_screenshot('Screenshots/docker_image_1.png')

    try:
        elem = driver.find_element_by_css_selector('[name="q"]')
    except:
        elem = driver.find_element_by_css_selector('[title="Поиск"]')

    elem.send_keys(name)
    elem.send_keys(Keys.RETURN)
    time.sleep(5)

    driver.save_screenshot('Screenshots/docker_image_2.png')
    time.sleep(5)

    try:
        driver.find_element_by_css_selector('[class="hdtb-mitem"]').click()
    except:
        driver.find_element_by_css_selector('[data-hveid="CAIQAw"]').click()
    time.sleep(5)
    driver.save_screenshot('Screenshots/docker_image_3.png')

    driver.close()
    driver.quit()
    print('pictures created')
