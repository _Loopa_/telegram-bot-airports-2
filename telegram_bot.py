import telebot
import subprocess
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from search import search_by_code
from db_Airport_template import Airport
from vincenty import vincenty
from telebot import types
from Airport_picture import airport_picture

Token = '1798291410:AAFXWdU60RMgnsXB-sJpOFhDthIl_tTzCtA'
bot = telebot.TeleBot(Token)

engine = create_engine('mysql://root:pass@mysql:3306/mydb', echo=True)
Session_search = sessionmaker(bind=engine)
s = Session_search()

code = ''
airport_name = ''
'''code_1 = {}
code_2 = {}'''


@bot.message_handler(content_types=['text'])
def start(message):
    if message.text == '/start':
        bot.send_message(message.from_user.id, "Hello. Input one code to receive info about airport, two - to receive "
                                               "distance between two airports")
        bot.register_next_step_handler(message, get_codes)

    else:
        bot.send_message(message.from_user.id, 'Call /start')


def get_codes(message):
    global code, airport_name
    # global code, code_1, code_2
    if len(message.text.split()) == 1:
        try:
            code = message.text
            info = search_by_code(message.text, Airport, s)
            airport_name = info['name']
            print(airport_name)

            bot.send_message(message.from_user.id, airport_name)

            keyboard = types.InlineKeyboardMarkup()  # наша клавиатура

            key_yes = types.InlineKeyboardButton(text='Yes', callback_data='yes')  # кнопка «Да»
            keyboard.add(key_yes)  # добавляем кнопку в клавиатуру
            key_no = types.InlineKeyboardButton(text='No', callback_data='no')
            keyboard.add(key_no)

            question = 'Do you want to receive picture of airport?';

            bot.send_message(message.from_user.id, text=question, reply_markup=keyboard)
            bot.register_next_step_handler(message, callback_worker);

        except TypeError:
            bot.send_message(message.from_user.id, "incorrect argument, input again")
            bot.register_next_step_handler(message, get_codes)

    elif len(message.text.split()) == 2:
        try:
            code_1 = search_by_code(message.text.split()[0], Airport, s)
            code_2 = search_by_code(message.text.split()[1], Airport, s)

            distance = vincenty([float(code_1['latitude']), float(code_1['longitude'])], [float(code_2['latitude']),
                                                                                          float(code_2['longitude'])])
            print("distance: ", distance)

            bot.send_message(message.from_user.id, 'distance: ' + str(distance))
            bot.send_message(message.from_user.id, "if you'd like to search distance again, call /start")

        except TypeError:
            bot.send_message(message.from_user.id, "incorrect arguments, input again")
            bot.register_next_step_handler(message, get_codes)

    else:
        bot.send_message(message.from_user.id, "too many arguments, input again")
        bot.register_next_step_handler(message, get_codes)


@bot.callback_query_handler(func=lambda call: True)
def callback_worker(call):
    if call.data == "yes":  # call.data это callback_data, которую мы указали при объявлении кнопки
        airport_picture(airport_name)

        img = open('Screenshots/docker_image_2.png', 'rb')
        bot.send_photo(call.message.chat.id, img)

        bot.register_next_step_handler(call.message, get_codes)

    elif call.data == "no":
        bot.send_message(call.from_user.id, "if you'd like to search distance again, call /start")
        bot.register_next_step_handler(call.message, get_codes)


print("The Telegram-script is running")
bot.polling(none_stop=True)
