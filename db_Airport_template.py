from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Airport(Base):
    __tablename__ = 'airports'
    id = Column(Integer, primary_key=True)
    ident = Column(String(50))
    tipe = Column(String(50))
    name = Column(String(200))
    elevation_ft = Column(String(20))
    continent = Column(String(10))
    iso_country = Column(String(20))
    iso_region = Column(String(20))
    municipality = Column(String(50))
    gps_code = Column(String(50))
    iata_code = Column(String(50))
    local_code = Column(String(50))
    latitude = Column(String(50))
    longitude = Column(String(50))

    def __init__(self, ident, tipe, name, elevation_ft, continent, iso_country, iso_region, municipality, gps_code,
                 iata_code, local_code, latitude, longitude):
        self.ident = ident
        self.tipe = tipe
        self.name = name
        self.elevation_ft = elevation_ft
        self.continent = continent
        self.iso_country = iso_country
        self.iso_region = iso_region
        self.municipality = municipality
        self.gps_code = gps_code
        self.iata_code = iata_code
        self.local_code = local_code
        self.latitude = latitude
        self.longitude = longitude

    def __repr__(self):
        return "<Airport('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')>" % (self.ident, self.tipe,
                                                                                           self.name, self.elevation_ft,
                                                                                           self.continent,
                                                                                           self.iso_country,
                                                                                           self.municipality,
                                                                                           self.gps_code,
                                                                                           self.iata_code,
                                                                                           self.local_code,
                                                                                           self.latitude,
                                                                                           self.longitude)