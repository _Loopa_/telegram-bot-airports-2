from http.server import HTTPServer, BaseHTTPRequestHandler
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from db_Airport_template import Airport

hostName = "0.0.0.0"
serverPort = 8000

engine = create_engine('mysql://root:pass@mysql:3306/mydb', echo=True)
Session_output = sessionmaker(bind=engine)
session = Session_output()


class MyServer(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes("<html><head><title>Python Airport Codes Server</title></head>", "utf-8"))
        self.wfile.write(bytes("<p>Request: %s</p>" % self.path, "utf-8"))
        self.wfile.write(bytes("<body>", "utf-8"))

        for airport_str in session.query(Airport).order_by(Airport.id)[1:6]:
            output_str = "<p>" + airport_str + "</p>"
            self.wfile.write(bytes(output_str))

        self.wfile.write(bytes("<p>This is an example web server.</p>", "utf-8"))
        self.wfile.write(bytes("</body></html>", "utf-8"))

    # def do_POST(self):


if __name__ == "__main__":
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
